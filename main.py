from gdal import ogr, osr
from rings.backYardFns import split_front_back
from rings.objDetFns import get_ml_pred_boxes_json
from rings.coco_json_boxes_to_shp import json_to_shp
from os.path import *
from rings.utils import *
import geopandas as gpd
def runFoliageModule(
        tif_path,
        parcelPth,
        building_path,
        corner_property_flag,
        extended_parcel_temp_dir,
        output_dir,
        obj_det_model_path,

):
    """

    Args:
        tif_path (string): Input tif image path
        parcelPth (string): Path to aoi
        building_path (string): path to building shape file
        corner_property_flag (int): flag = 2 for corner property
        extended_parcel_temp_dir (string): Path to dir output of extended parcel
        output_dir (string): Path to dir to save final rings shape file
        obj_det_model_path (string): Path to obj det model
    Output:
        Saves the rings shape file in output_dir
    """
    '''
    get front edge and road shps
    use front edge/road to get front yard parcel
    use front yard parcel to clip the tif image
    run obj det model on clipped front yard tif
    get model output as boxes shape file
    run post process on boxes vectors
    draw 4ft diameter rings at center of boxes and save only rings
    give output rings as final output
    '''
    # base_dir = extended_parcel_dir#"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/Test/Results/extended_parcels/0"

    # parcelPth = join(base_dir,'extended_parcel.shp')
    # parcel_aoi_path = "/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/Test/Tree_cover_Test/aois_clipped/0.shp"
    # name = parcel_aoi_path.split("/")[-1]
    temp_dir = extended_parcel_temp_dir
    roadPth = join(temp_dir,'temp_files',f"selected_roads.shp")
    front_edge_path = join(temp_dir,'temp_files',f"front_edges.shp")
    # building_path = "/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/Test/Results/L_B_BD_V2/epoch_385/0_pred.shp"
    front_back_aoi_dir = output_dir

    ### Split Front Yard Parcel ####

    split_front_back(parcelPth = parcelPth,
                    roadPth = roadPth,
                    building_path = building_path,
                    front_edge_path=front_edge_path,
                    front_back_aoi_dir=front_back_aoi_dir,
                    corner_property_flag=corner_property_flag)

    ### Clip the image with front yard parcel ####
    front_yard_parcel = join(front_back_aoi_dir, basename(parcelPth)).replace(".shp", "_front.shp")
    front_yard_clipped_tif_path = join(front_back_aoi_dir,basename(tif_path).replace(".tif", "_front.tif"))

    clip_tif(tif_path, front_yard_parcel, front_yard_clipped_tif_path)

    #### Get Box Model Predictions

    boxes_json = get_ml_pred_boxes_json(obj_det_model_path,tif_path,front_back_aoi_dir)
    boxes_shp_path = json_to_shp(tif_path,boxes_json,front_back_aoi_dir)

    foliage_shp_path = join(front_back_aoi_dir, 'rings.shp')

    if boxes_shp_path:
        rings_df = gpd.read_file(boxes_shp_path)
        front_parcel_df = gpd.read_file(front_yard_parcel)
        rings_df.loc[:,'FlgPrclRto'] = rings_df.area/front_parcel_df.area.max()
        pred_foliage = rings_df[(rings_df.confidence>=0.95) & (rings_df.FlgPrclRto<=0.3) & (rings_df.l_w_ratio <= 1.75)]
        if pred_foliage.shape[0]:
            pred_foliage = pred_foliage.to_crs("epsg:2249")
            pred_foliage.loc[:,'geometry'] = pred_foliage.centroid.buffer(2.5)
            pred_foliage.to_file(foliage_shp_path)
        else:
            epsg_4326_srs = osr.SpatialReference()
            epsg_4326_srs.ImportFromEPSG(4326)
            driver = ogr.GetDriverByName("ESRI Shapefile")
            dataSource = driver.CreateDataSource(foliage_shp_path)
            layer = dataSource.CreateLayer("empty", epsg_4326_srs)

    return foliage_shp_path
