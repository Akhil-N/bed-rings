import json,os
import rasterio
from PIL import Image
from shapely.geometry import Polygon
import geopandas as gpd
from glob import glob
from tqdm import tqdm
from os.path import basename

class ImageObj(object):
    def __init__(self, TL_x, TL_y, BR_x, BR_y, image):
        self.TL_x = TL_x
        self.TL_y = TL_y
        self.BR_x = BR_x
        self.BR_y = BR_y
        self.image = image
        self.width,self.height = image.size
        
def get_image_object(img_path):

    raster_img = rasterio.open(img_path)
    TL_x = raster_img.bounds.left
    TL_y = raster_img.bounds.top
    BR_x = raster_img.bounds.right
    BR_y = raster_img.bounds.bottom

    img = Image.open(img_path)
    image_obj = ImageObj(TL_x, TL_y, BR_x, BR_y, img)
    return image_obj

def convert_pxl_coords_to_geo_coords(image_obj, bx_coords):
    '''
    (x1_geo - TL_x) / x_res = x1_px
    x1_geo = (x1_px * x_res) + TL_x
    '''
    TL_x, TL_y, BR_x, BR_y, width, height = image_obj.TL_x, image_obj.TL_y, image_obj.BR_x, image_obj.BR_y, image_obj.width, image_obj.height
    geo_tuple_list = []
    x_res = (BR_x - TL_x) / width
    y_res = (BR_y - TL_y) / height
    for i in range(len(bx_coords)):
        try:
            #xmn,ymn,w,h
            x1, y1, x2, y2 = bx_coords[i]
            x2,y2 = x1+x2,y1+y2
            # pixel_tuple_list += [((x1 - TL_x) / x_res, (y1 - TL_y) / y_res, (x2 - TL_x) / x_res, (y2 - TL_y) / y_res)]
            geo_tuple_list += [((x1 * x_res) + TL_x, (y1 * y_res) + TL_y, (x2 * x_res) + TL_x, (y2 * y_res) + TL_y)]
        except ValueError as e:
            print(e)
            print(bx_coords[i])
    return geo_tuple_list

def json_to_shp(img_path,ann_path,save_dir):
    shape_path = os.path.join(save_dir,"boxes.shp")
    img_obj = get_image_object(img_path)
    d = json.load(open(ann_path))
    coco_bboxes = [ann['bbox'] for ann in d['annotations']]
    confidences = [ann['confidence'] for ann in d['annotations']]
    geo_boxes = convert_pxl_coords_to_geo_coords(img_obj, coco_bboxes)
    box_polys = [Polygon([(bx[0],bx[1]),(bx[2],bx[1]),(bx[2],bx[3]),(bx[0],bx[3])]) for bx in geo_boxes]
    df = gpd.GeoDataFrame({'geometry':box_polys,'confidence':confidences},crs=4326)
    if df.shape[0]:
        df.to_file(shape_path)
        return shape_path
    else: return None

# if __name__ == '__main__':
#     pred_jsons = glob("/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/V3_FrointYard/Test_2k_Results/eff_b7_340/*.json")
#     out_box_shapes_dir = "/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/V3_FrointYard/Test_2k_Results/eff_b7_340_shps"
#     os.makedirs(out_box_shapes_dir,exist_ok=True)
#     for ann_path in tqdm(pred_jsons):
#         im_nm = basename(ann_path).split(".")[0]
#         img_path = f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/V3_FrointYard/Test_2k/clipped_tifs_front/{im_nm}.tif"
        
#         shape_path = f"{out_box_shapes_dir}/{im_nm}.shp"

#         img_obj = get_image_object(img_path)

#         d = json.load(open(ann_path))
#         coco_bboxes = [ann['bbox'] for ann in d['annotations']]
#         confidences = [ann['confidence'] for ann in d['annotations']]
#         geo_boxes = convert_pxl_coords_to_geo_coords(img_obj, coco_bboxes)
#         box_polys = [Polygon([(bx[0],bx[1]),(bx[2],bx[1]),(bx[2],bx[3]),(bx[0],bx[3])]) for bx in geo_boxes]
#         gpd.GeoDataFrame({'geometry':box_polys,'confidence':confidences},crs=4326).to_file(shape_path)
#         # gpd.GeoDataFrame({'geometry':box_polys},crs=4326).to_file(shape_path)