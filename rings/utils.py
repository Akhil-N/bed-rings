import os
def clip_tif(tif_path, shp_path, out_tif_path):
    try:
        os.system(
            "gdalwarp -q -of GTiff -cutline {} -crop_to_cutline {} {}".format(
                shp_path, tif_path, out_tif_path
            )
        )
        # print('generating ', out_tif_path)
    except Exception as e:
        print(e)
        print("some issue in ", tif_path)