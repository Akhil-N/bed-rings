import torchvision, json, torch
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator
from os.path import *
from PIL import Image
from torchvision import transforms as T

def get_transform():
    transforms = []
    transforms.append(T.ToTensor())
    return T.Compose(transforms)

def get_model_pred_boxes(model,img_path,iou_threshold=0,device="cuda"):
    imgI = Image.open(img_path).convert("RGB")
    img_width,img_height = imgI.size
    output = model([get_transform()(imgI).to(device)])
    bxs=[]
    for n,_ in enumerate(output):
        if iou_threshold:
            indcs = torchvision.ops.nms(output[n]['boxes'],output[n]['scores'],iou_threshold=iou_threshold)
        else:
            indcs = range(len(output[n]['boxes']))
        for i in indcs:
            bx = output[n]['boxes'][i]
            bx = [int(bx[0]), int(bx[1]), int(bx[2]), int(bx[3])]
            bx_dct = {
                'confidence' : float(output[n]['scores'][i]),
                'box': [int(bx[0]), int(bx[1]), int(bx[2]), int(bx[3])],
                'img_width':img_width,
                'img_height':img_height,
            }
            bxs.append(bx_dct)
    return bxs

def save_coco_annotations(tif_path,pxl_bxs,coco_bbx_dir):
    n = 0
    if len(pxl_bxs):
        im_width,im_height = pxl_bxs[0]['img_width'],pxl_bxs[0]['img_height']
        ann = {}
        ann['categories'] = [{'id': 1,
                            'name': 'Foliage',
                            'supercategory': None,
                            'metadata': {},
                            'color': '#fa4697'}]
        ann['images'] = [{'id': n,
                        'width': im_width,
                        'height': im_height,
                        'file_name': basename(tif_path),
                        'path': tif_path,
                        'license': None,
                        'fickr_url': None,
                        'coco_url': None,
                        'date_captured': None,
                        'metadata': {}}]
        ann['annotations'] = []
        for bx_id,box_dict in enumerate(pxl_bxs):
            x1,y1,x2,y2= box_dict['box']
            bx_w,bx_h = x2-x1,y2-y1
            annotation = {'id': bx_id,
                            'image_id': n,
                            'category_id': 1,
                            'width': im_width,
                            'height': im_height,
                            'area': bx_w*bx_h,
                            'segmentation': [[x1,y1,x2,y2]],
                            'bbox': [x1,y1,bx_w,bx_h],
                            'confidence': box_dict['confidence'],
                            'metadata': {},
                            'color': '#e3ed16',
                            'iscrowd': 0,
                            'isbbox': True}
            ann['annotations'].append(annotation)

        file_path = join(coco_bbx_dir,"boxes.json")
        with open(file_path, 'w') as fp:
            json.dump(ann, fp)
        return file_path

def get_resnext_frcnn_model2(num_classes,pretrained=False):
    resnet = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnext101_32x8d',pretrained=pretrained)
    anchor_sizes = ((10,25,50,65),(55,80,105,120),(95,125,160,200),(180,220,250,350),(300,350,400,500))
    aspect_ratios = ((0.5,0.8,1.2,2.5),) * len(anchor_sizes)
    anchor_generator = AnchorGenerator(sizes=anchor_sizes, aspect_ratios=aspect_ratios)
    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0','1','2','pool'],
                                                    output_size=16,
                                                    sampling_ratio=2)
    model = FasterRCNN(resnet,
                    num_classes=num_classes,
                    min_size= 1024,
                    max_size=1024,
                    rpn_anchor_generator=anchor_generator,
                    box_roi_pool=roi_pooler,
                    rpn_pre_nms_top_n_train = 3000,
                    rpn_pre_nms_top_n_test = 2000,
                    rpn_post_nms_top_n_train = 3000,
                    rpn_post_nms_top_n_test = 2000,
                    rpn_batch_size_per_image = 720,
                    box_detections_per_img = 50,
                    box_batch_size_per_image = 720,
                    box_score_thresh=0.1)
    return model

def load_pretrained_model(model_path,num_classes=2,device = 'cuda',model_name = None):
    if model_name == 'resnext-frcnn2':
        model = get_resnext_frcnn_model2(num_classes,pretrained=False)
    else:
        print(f"Model {model_name} not Supported")
        return
    model.load_state_dict(torch.load(model_path)['model'])
    model.to(device)
    return model

def get_ml_pred_boxes_json(model_path,img_path,pred_coco_save_dir):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # torch.cuda.set_device(gpu_id)
    model = load_pretrained_model(model_path,device = device,model_name='resnext-frcnn2')
    boxes = get_model_pred_boxes(model,img_path)
    boxes_json = save_coco_annotations(img_path,boxes,pred_coco_save_dir)
    return boxes_json